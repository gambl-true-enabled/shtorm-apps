package com.firekasj.dfendo

import android.content.Context
import android.webkit.JavascriptInterface

private const val SHTORM_TABLE = "com.SHTORM.table.123"
private const val SHTORM_ARGS = "com.SHTORM.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(SHTORM_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(SHTORM_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(SHTORM_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(SHTORM_ARGS, null)
}
