package com.firekasj.dfendo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager

class StormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_storm)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        supportFragmentManager.beginTransaction().replace(R.id.main_container, AppsFragment())
            .commit()
    }
}